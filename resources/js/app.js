require('./bootstrap');

import { createApp } from 'vue';
import Vue from 'vue';
import './../css/app.css';

import router from "./router";

import App from "./App.vue";

import RegisterComponent from "./components/conductor/RegisterComponent.vue";
import SideBar from "./components/conductor/SideBar.vue";
import TopBar from "./components/conductor/TopBar.vue";
import Dashboard from "./components/conductor/Dashboard.vue";
import ListPassagers from "./components/conductor/ListPassagers.vue";
import CreateTraject from "./components/conductor/CreateTraject.vue";
import ListTraject from "./components/conductor/ListTraject.vue";
import Content from "./components/conductor/Content.vue";

const app = createApp(App);

app.component('register', RegisterComponent);
app.component('side-bar', SideBar);
app.component('top-bar', TopBar);
app.component('conductor-dashboard', Dashboard);
app.component('list-passagers', ListPassagers);
app.component('create-traject', CreateTraject);
app.component('list-traject', ListTraject);
app.component('content', Content);
// app.component('login', RegisterComponent);
app.use(router).mount("#app");