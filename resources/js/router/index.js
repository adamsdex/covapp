import { createRouter, createWebHistory } from "vue-router";

import HomeComponent from "../HomeComponent.vue";
import RegisterComponent from "../components/conductor/RegisterComponent.vue";
import Dashboard from "../components/conductor/Dashboard.vue";
import ListTraject from "../components/conductor/ListTraject.vue";
import CreateTraject from "../components/conductor/CreateTraject.vue";
import ListPassagers from "../components/conductor/ListPassagers.vue";
import Content from "../components/conductor/Content.vue";

const routes = [
    { 
        path: '/',
        component: HomeComponent,
        name: 'index'
    },
    {
        path: '/register',
        component: RegisterComponent,
        name: 'conductor.register'
    },
    {
        path: '/login',
        component: RegisterComponent,
        name: 'conductor.login'
    },
    {
        path: '/conductor/dashboard',
        component: Dashboard,
        name: 'conductor.dashboard'
    },
    {
        path: '/conductor/dashboard/content',
        component: Content,
        name: 'conductor.dashboard.content'
    },
    {
        path: '/conductor/dashboard/create-traject',
        component: CreateTraject,
        name: 'conductor.dashboard.createTraject'
    },
    {
        path: '/conductor/dashboard/all-trajects',
        component: ListTraject,
        name: 'conductor.dashboard.allTrajects'
    },
    {
        path: '/conductor/dashboard/all-trajects-index',
        component: ListTraject,
    },
    {
        path: '/conductor/dashboard/all-passagers',
        component: ListPassagers,
        name: 'conductor.dashboard.allPassagers'
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;