<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CovApp</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        
        <!-- Scripts -->
        <script
        src="https://kit.fontawesome.com/64d58efce2.js"
        crossorigin="anonymous"
        ></script>

    </head>
    <body id="app">
       <script src="{{ asset('js/app.js') }}"></script>
       <script>
            const sign_in_btn = document.querySelector("#sign-in-btn");
            const sign_up_btn = document.querySelector("#sign-up-btn");
            const container = document.querySelector(".container");
            
            sign_up_btn.addEventListener("click", () => {
                container.classList.add("sign-up-mode");
            });
            
            sign_in_btn.addEventListener("click", () => {
                container.classList.remove("sign-up-mode");
            });
       </script>
       <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    </body>
</html>
