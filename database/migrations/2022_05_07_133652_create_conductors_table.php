<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConductorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conductors', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('Prenom');
            $table->string('email')->unique();
            $table->bigInteger('telephone')->unique();
            $table->date('date_naissance');
            $table->string('sexe');
            $table->string('photo_profil_path');
            $table->string('photo_permis_path');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

}
