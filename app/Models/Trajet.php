<?php

namespace App\Models;

use App\Models\Conductor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Trajet extends Model
{
    use HasFactory;

    public function conducteur()
    {
        return $this->belongsTo(Conductor::class);
    }
}
