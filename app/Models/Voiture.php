<?php

namespace App\Models;

use App\Models\Conductor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Voiture extends Model
{
    use HasFactory;
        /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nb_places',
        'photo_voiture_path',
        'photo_facture_achat_path'
    ];

    public function conductor()
    {
        return $this->belongsTo(Conductor::class);
    }
}
