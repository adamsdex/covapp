<?php

namespace App\Http\Controllers;

use App\Models\Trajet;
use Illuminate\Http\Request;

class TrajetController extends Controller
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all_trajects()
    {
        $traject = Trajet::all();
        return response()->json($traject);
        // return view('auth.conductor.dashboard');
    }

    public function all_trajects_index()
    {
        // $traject = Trajet::all();
        // return response()->json($traject);
        return view('auth.conductor.dashboard');
    }

    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_traject()
    {
        return view('auth.conductor.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create_trajects(Request $request)
    {
        $request->validate([
            'point_depart' => 'required',
            'point_arrivee' => 'required',
            'date_depart' => 'required',
            'points_arret'
        ]);

        $trajet = new Trajet;
        $trajet->point_depart = $request->point_depart;
        $trajet->point_arrivee = $request->point_arrivee;
        $trajet->date_depart = $request->date_depart;
        $trajet->save();

        return redirect()->route('conductor.dashboard.allTrajects');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trajet  $Trajet
     * @return \Illuminate\Http\Response
     */
    public function show(Trajet $Trajet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trajet  $Trajet
     * @return \Illuminate\Http\Response
     */
    public function edit(Trajet $Trajet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trajet  $Trajet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trajet $Trajet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trajet  $Trajet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trajet $Trajet)
    {
        //
    }
}
