<?php

namespace App\Http\Controllers\Auth;

use App\Models\Voiture;
use App\Models\Conductor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_login_form()
    {
        return view('auth.conductor.register');
    }

    public function process_login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->except(['_token']);
        $conductor = Conductor::where('email', $request->email)->first();

        if (auth()->attempt($credentials)) {
           return redirect()->route('auth.conductor.dashboard');
        }
        else{
            session()->flash('message', 'Identifiants invalides');
            return redirect()->back();
        }
    }



    public function show_signup_form()
    {
        return view('auth.conductor.register');
    }

    public function process_signup(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'date_naissance' => 'required',
            'sexe' => 'required',
            'photo_profil_path' => 'required|mimes:png,jpg,jpeg|max:5048',
            'photo_permis_path' => 'required|mimes:png,jpg,jpeg|max:5048',
            'photo_voiture_path' => 'required|mimes:png,jpg,jpeg|max:5048',
            'photo_facture_achat_path' => 'required|mimes:png,jpg,jpeg|max:5048',
            'nb_places' => 'required',
            'password' => 'required',
        ]);
        
        $photo_profil = $request->file('photo_profil_path');
        $new_photo_profil_name = time() . '-' . $request->nom . '-' . 'profile.' . $photo_profil->getClientOriginalExtension();

        $photo_permis = $request->file('photo_permis_path');
        $new_photo_permis_name = time() . '-' . $request->nom . '-' . 'permis.' . $photo_permis->getClientOriginalExtension();;

        $photo_profil->move(public_path('images/conductor/profile'), $new_photo_profil_name);
        $photo_permis->move(public_path('images/conductor/profile'), $new_photo_permis_name);
        $photo_profil_path = asset('images/conductor/profile').$new_photo_profil_name;
        $photo_permis_path = asset('images/conductor/profile').$new_photo_permis_name;


        $photo_voiture = $request->file('photo_voiture_path');
        $new_photo_voiture_name = time() . '-' . $request->nom . '-' . 'voiture.' . $photo_voiture->getClientOriginalExtension();

        $photo_facture_achat = $request->file('photo_facture_achat_path');
        $new_photo_facture_achat_name = time() . '-' . $request->nom . '-' . 'FactureAchat.' . $photo_facture_achat->getClientOriginalExtension();

        $photo_voiture->move(public_path('images/conductor/voiture/'), $new_photo_voiture_name);
        $photo_facture_achat->move(public_path('images/conductor/voiture/'), $new_photo_facture_achat_name);

        $conductor = new Conductor;
        $conductor->nom = $request->nom;
        $conductor->prenom = $request->prenom;
        $conductor->telephone = $request->telephone;
        $conductor->email = $request->email;
        $conductor->date_naissance = $request->date_naissance;
        $conductor->sexe = $request->sexe;
        $conductor->photo_profil_path = $new_photo_profil_name;
        $conductor->photo_permis_path = $new_photo_permis_name;
        $conductor->password = Hash::make($request->password);
        $conductor->save();
        $last_id = $conductor->id;

        $voiture = new Voiture;
        $voiture->photo_voiture_path = $new_photo_voiture_name;
        $voiture->photo_facture_achat_path = $new_photo_facture_achat_name;
        $voiture->nb_places = $request->nb_places;
        $voiture->conductor_id = $last_id;
        $voiture->save();
        
        return redirect()->route('conductor.dashboard');
    }

    public function show_dashboard()
    {
        return view('auth.conductor.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conductor  $conductor
     * @return \Illuminate\Http\Response
     */
    public function show(Conductor $conductor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conductor  $conductor
     * @return \Illuminate\Http\Response
     */
    public function edit(Conductor $conductor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conductor  $conductor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conductor $conductor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conductor  $conductor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conductor $conductor)
    {
        //
    }
}
