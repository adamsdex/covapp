<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PassagerController;
use App\Http\Controllers\TrajetController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 

// Route::get('/register', function () {
//     return view('auth.conductor.register');
// });
// Route::get('/login', function () {
//     return view('auth.conductor.register');
// });



Route::get('/register',[LoginController::class,'show_signup_form']);
Route::post('/register',[LoginController::class,'process_signup']);


Route::get('/login',[LoginController::class,'show_login_form']);
Route::post('/login',[LoginController::class,'process_login']);


Route::get('/conductor/dashboard',[LoginController::class,'show_dashboard'])->name('conductor.dashboard');
Route::get('/conductor/dashboard/content',[LoginController::class,'show_dashboard'])->name('conductor.dashboard.content');


Route::get('/conductor/dashboard/create-traject',[TrajetController::class,'create_traject'])->name('conductor.dashboard.createTraject');
Route::post('/conductor/dashboard/create-traject',[TrajetController::class,'create_trajects']);
Route::get('/conductor/dashboard/all-trajects-index',[TrajetController::class,'all_trajects_index']);
Route::get('/conductor/dashboard/all-trajects',[TrajetController::class,'all_trajects'])->name('conductor.dashboard.allTrajects');
Route::get('/conductor/dashboard/traject/{id}',[TrajetController::class,'show_traject'])->name('conductor.dashboard.showTraject');
Route::get('/conductor/dashboard/all-passagers',[PassagerController::class,'all-passagers'])->name('conductor.dashboard.allPassagers');
