module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
    "./resources/js/components/conductor/*.vue",
  ],
  theme: {
    extend: {},
  },
  plugins: [ 
    require('@tailwindcss/aspect-ratio'),
            ],
}
